﻿using Graph_2.Models;
using Graph_2.Services;
using Graph_2.Types;
using GraphQL.Resolvers;
using GraphQL.Types;
using GraphQL.Utilities.Federation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Subscription
{
    public class PosterSubscription:ObjectGraphType
    {
        private readonly IEventService _eventService;

        public PosterSubscription(IEventService eventService)
        {
            _eventService = eventService;

            Name = nameof(PosterSubscription);

            AddField(new EventStreamFieldType
            {
                Name="posterCreated",
                Type=typeof(EventDataType),
                Resolver=new FuncFieldResolver<EventDataModel>(context => context.Source as EventDataModel),
                Subscriber= new EventStreamResolver<EventDataModel>(context =>
                {
                    return _eventService.OnCreateObservable;
                })
            });
        }
    }
}
