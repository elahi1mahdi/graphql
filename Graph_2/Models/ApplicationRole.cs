﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DomainClasses.Role
{
    public class ApplicationRole:IdentityRole<string>
    {
        public ApplicationRole()
        {}
    
        [StringLength(100)]
        [Display(Name =("وظیفه"))]
        public string Task { get; set; }
        [StringLength(100)]
        [Display(Name = ("مسئولیت"))]

        public string Responsibilty { get; set; }
        [Display(Name = ("توضیحات"))]

        public string Description { get; set; }
        [Required]
        [Display(Name = ("وضعیت "))]

        public byte Role_Status { get; set; }
        [Display(Name = ("نوع "))]

        public byte Role_Type { get; set; }

       
        public string Access { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override string Id { get => base.Id; set => base.Id = value; }

    }
   
    
}
