﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;

namespace Graph_2.Models
{
    public class EventDataModel
    {
        public int PosterId { get; set; }
        public EventDataModel(int posterId)
        {
            PosterId = posterId;
        }
    }
}
