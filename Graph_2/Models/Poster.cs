﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Models
{
    public class Poster
    {
        [Key]
        public int Poster_Id { get; set; }
        [Required]
        public string Title { get; set; }
        [EmailAddress]
        [StringLength(10,ErrorMessage ="title is required")]
        public string  Email{ get; set; }
        public string  Description { get; set; }
        
        public int  Price { get; set; }
        public DateTime Date { get; set; }

        [ForeignKey("PosterGroup")]
        public int PosterGroup_Id { get; set; }
        public PosterGroup PosterGroup { get; set; }
    }
}
