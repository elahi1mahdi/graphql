﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Models
{
    public class PageResponse<T>
    {
        public List<T> Nodes { get; set; }
        public int TotalCount { get; set; }
        public bool HasNextPage { get; set; }
        public bool HasPerviousPage { get; set; }
    }
}
