﻿using GraphQL.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Graph_2.Models.Auth
{
    public class GraphQLUserContext : Dictionary<string,object> , IProvideClaimsPrincipal
    {
        public ClaimsPrincipal User { get; set; }
    }
}
