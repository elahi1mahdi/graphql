﻿using GraphQL.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Graph_2.Models.Auth
{
    public class AuthValidationRule : IValidationRule
    {
        public INodeVisitor Validate(ValidationContext context)
        {
            var userContext = context.UserContext as ClaimsPrincipal;
            var authenticated = userContext?.Identity?.IsAuthenticated ?? false;


            return new EnterLeaveListener(_ =>
            {
                if (!authenticated)
                {
                    context.ReportError(new ValidationError(
                        context.OriginalQuery,
                        "احراز هویت",
                        "شما به این بخش دسترسی ندارید "
                        ));
                }
            });
        }
    }
    public class TokenViewModel
    {
        public string  UserId { get; set; }
        public string Token { get; set; }
        public DateTime  ExpireOn { get; set; }
    }
}
