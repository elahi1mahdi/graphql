﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Models
{
    public class PosterGroup
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual IEnumerable<Poster> Posters { get; set; }
    }
}
