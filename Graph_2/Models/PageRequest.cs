﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace Graph_2.Models
{
    #region OrderBy
    public enum SortingDirection
    {
        ASC, DESC
    }
    public class SortingDetails<T>
    {
        public T Field { get; set; }
        public SortingDirection Direction { get; set; }
    }
   

    public enum CompletedPosterSortingFiedls
    {
        Id,
        Title,
        Price
    }
    #endregion

    #region Filter
    public class FilteringDetails<T>
    {
        public T Field { get; set; }
        public string Value { get; set; }
    }

    public enum CompletedPosterFilterFiedls
    {
        Id,
        Title,
        Price,
        Description,
        PosterGroup_Id
    }
    #endregion
    public class PageRequest
    {
        public int? First { get; set; }
        public int? Last { get; set; }
        public string After { get; set; }
        public string Before { get; set; }

        public List<SortingDetails<CompletedPosterSortingFiedls>> OrderBy { get; set; }
        public List<FilteringDetails<CompletedPosterFilterFiedls>> FilterBy { get; set; }


    }
}
