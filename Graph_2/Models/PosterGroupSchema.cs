﻿using Graph_2.Mutaions;
using Graph_2.Query;
using Graph_2.Subscription;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Models
{
    public class PosterGroupSchema : Schema
    {
        public PosterGroupSchema(IServiceProvider services):base(services)
        {
            Services = services;
            Query = (PosterGroupQuery)services.GetService(typeof(PosterGroupQuery));
            //Mutation = (PosterMutaion)services.GetService(typeof(PosterMutaion));
            //Subscription = (PosterSubscription)services.GetService(typeof(PosterSubscription));
        }
    }
}
