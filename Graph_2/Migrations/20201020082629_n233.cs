﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Graph_2.Migrations
{
    public partial class n233 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PosterGroup_Id",
                table: "Poster_tb",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "PosterGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PosterGroup", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Poster_tb_PosterGroup_Id",
                table: "Poster_tb",
                column: "PosterGroup_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Poster_tb_PosterGroup_PosterGroup_Id",
                table: "Poster_tb",
                column: "PosterGroup_Id",
                principalTable: "PosterGroup",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Poster_tb_PosterGroup_PosterGroup_Id",
                table: "Poster_tb");

            migrationBuilder.DropTable(
                name: "PosterGroup");

            migrationBuilder.DropIndex(
                name: "IX_Poster_tb_PosterGroup_Id",
                table: "Poster_tb");

            migrationBuilder.DropColumn(
                name: "PosterGroup_Id",
                table: "Poster_tb");
        }
    }
}
