﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Graph_2.Migrations
{
    public partial class n2333 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Poster_tb_PosterGroup_PosterGroup_Id",
                table: "Poster_tb");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PosterGroup",
                table: "PosterGroup");

            migrationBuilder.RenameTable(
                name: "PosterGroup",
                newName: "PosterGroup_tb");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PosterGroup_tb",
                table: "PosterGroup_tb",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Poster_tb_PosterGroup_tb_PosterGroup_Id",
                table: "Poster_tb",
                column: "PosterGroup_Id",
                principalTable: "PosterGroup_tb",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Poster_tb_PosterGroup_tb_PosterGroup_Id",
                table: "Poster_tb");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PosterGroup_tb",
                table: "PosterGroup_tb");

            migrationBuilder.RenameTable(
                name: "PosterGroup_tb",
                newName: "PosterGroup");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PosterGroup",
                table: "PosterGroup",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Poster_tb_PosterGroup_PosterGroup_Id",
                table: "Poster_tb",
                column: "PosterGroup_Id",
                principalTable: "PosterGroup",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
