﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Helper
{
    public static class Constant
    {
        public static class Roles
        {
            public static string Customer="Customer";
            public static string Admin="Admin";
        }
        public static class AuthPolicy
        {
            public static string CustomerPolicy = "CustomerPolicy";
            public static string AdminPolicy = "AdminPolicy";
        }
    }
}
