﻿using Graph_2.Enums;
using Graph_2.Helper;
using Graph_2.Models;
using Graph_2.Services;
using Graph_2.Types;
using GraphQL.Types;
using GraphQL.Types.Relay.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Graph_2.Query
{
    public class PosterGroupQuery : ObjectGraphType

    {
        public PosterGroupQuery(IPosterGroupService posterGroupService)
        {
            Name = nameof(PosterGroupQuery);

            FieldAsync<ListGraphType<PosterGroupType>>(
                name: "posterGroups",
                resolve: async context => await posterGroupService.GetAll());

            Field<PosterGroupType>(
          name: "posterGroupById",
          arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
          resolve:  context =>  posterGroupService.GetById(context.GetArgument<int>("id")));


        }
    }
}
