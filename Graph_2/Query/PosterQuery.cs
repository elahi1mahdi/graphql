﻿using Graph_2.Enums;
using Graph_2.Helper;
using Graph_2.Models;
using Graph_2.Services;
using Graph_2.Types;
using GraphQL.Authorization;
using GraphQL.Types;
using GraphQL.Types.Relay.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Graph_2.Query
{
    public class PosterQuery : ObjectGraphType

    {
        public PosterQuery(IPosterService posterService, IPosterGroupService posterGroupService)
        {
            Name = nameof(PosterQuery);

            #region Poster Queries

            FieldAsync<ListGraphType<PosterType>>(
                 name: "posters",
                 resolve: async context => await posterService.GetAll()).AuthorizeWith(Constant.AuthPolicy.CustomerPolicy);

            FieldAsync<ListGraphType<PosterType>>(
              name: "postersByDate",
            arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "startDate" }, 
            new QueryArgument<StringGraphType> { Name = "endDate" }),
                 resolve: async context => await posterService.GetPostersByDate(context.GetArgument<string>("startDate"), context.GetArgument<string>("endDate")));

            FieldAsync<PosterType>(
            name: "posterById",
            arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
            resolve: async context => await posterService.GetById(context.GetArgument<int>("id"))).AuthorizeWith(Constant.AuthPolicy.AdminPolicy); ;

            FieldAsync<ListGraphType<PosterType>>(
          name: "groupPosters",
          arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" },
          new QueryArgument<ListGraphType<CompletedPosterFilterByInputType>> { Name = "filterPosters" }),
          resolve: async context => await posterService.GetPostersByGroupId(context.GetArgument<int>("id"), context.GetArgument<List<FilteringDetails<CompletedPosterFilterFiedls>>>("filterPosters")));

            Connection<PosterType>()
                .Name("completedPosters")
                .Unidirectional()
                .PageSize(10)
                .Argument<ListGraphType<CompletedPosterOrderByInputType>>("orderBy", "Pass feild & direction")
                .Argument<ListGraphType<CompletedPosterFilterByInputType>>("filterBy", "Pass feild ")
                .ResolveAsync(async context =>
                {
                    var pageRequest = new PageRequest
                    {
                        First = context.First,
                        Last = context.Last,
                        After = context.After,
                        Before = context.Before,
                        OrderBy = context.GetArgument<List<SortingDetails<CompletedPosterSortingFiedls>>>("orderBy"),
                        FilterBy = context.GetArgument<List<FilteringDetails<CompletedPosterFilterFiedls>>>("filterBy"),
                    };
                    var pageResponse = await posterService.GetCompeletePostersAsync(pageRequest);

                    (string startCursor, string lastCursor) = CursorHelper.GetFirstAndLastCursor(pageResponse.Nodes.Select(x => x.Poster_Id));

                    var edge = pageResponse.Nodes.Select(x => new Edge<Poster>
                    {
                        Cursor = CursorHelper.ToCursor(x.Poster_Id),
                        Node = x
                    }).ToList();

                    var connection = new Connection<Poster>()
                    {
                        Edges = edge,
                        TotalCount = pageResponse.TotalCount,
                        PageInfo = new PageInfo
                        {
                            HasNextPage = pageResponse.HasNextPage,
                            HasPreviousPage = pageResponse.HasPerviousPage,
                            StartCursor = startCursor,
                            EndCursor = lastCursor
                        }
                    };
                    return connection;

                });

            #endregion


            #region PosterGroup Queries
            FieldAsync<ListGraphType<PosterGroupType>>(
                   name: "posterGroups",
                   resolve: async context => await posterGroupService.GetAll());


            Field<PosterGroupType>(
       name: "posterGroupById",
       arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
       resolve: context => posterGroupService.GetById(context.GetArgument<int>("id")));
          
            #endregion

        }
    }
}
