﻿using DataLayer;
using Graph_2.Helper;
using Graph_2.Models;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Graph_2.Services
{
    public interface IPosterService
    {
        Task<Poster> Add(Poster poster);
        Task<IEnumerable<Poster>> GetAll();
        Task<IEnumerable<Poster>> GetPostersByGroupId(int id,List<FilteringDetails<CompletedPosterFilterFiedls>> filter);
        Task<Poster> GetById(int id);
        Task<PageResponse<Poster>> GetCompeletePostersAsync(PageRequest request);
         Task<string> Remove(int id);
        Task<string> Update(Poster poster);
        Task<IEnumerable<Poster>> GetPostersByDate(string startDate,string endDate);

        Task<ILookup<int, Poster>> GetPostersByGroupIds(IEnumerable<int> groupIds);

    }
    public class PosterService : IPosterService
    {
        private readonly ApplicationDbContext _db;
        private readonly IEventService _eventService;
        public PosterService(ApplicationDbContext db, IEventService eventService)
        {
            _eventService = eventService;
            _db = db;
        }
        public async Task<IEnumerable<Poster>> GetPostersByDate(string startDate, string endDate)
        {
            var st_date = Convert.ToDateTime(startDate);
            var en_date = Convert.ToDateTime(endDate);

            return await _db.Poster_tb.Where(x=>x.Date >= st_date && x.Date <= en_date).ToListAsync();
        }
        public async Task<IEnumerable<Poster>> GetAll()
        {
            return await _db.Poster_tb.ToListAsync();
        }
        public async Task<IEnumerable<Poster>> GetPostersByGroupId(int id, List<FilteringDetails<CompletedPosterFilterFiedls>> filter)
        {
            var query =  _db.Poster_tb.Where(x => x.PosterGroup_Id == id).AsQueryable();

            foreach (var item in filter)
            {
                if(item.Field==CompletedPosterFilterFiedls.Title)
                {
                   query= query.Where(x => x.Title.Contains(item.Value));
                }
                else if(item.Field==CompletedPosterFilterFiedls.Price)
                {
                    query = query.Where(x => x.Price == Convert.ToInt32(item.Value));
                }
            }

            return await query.ToListAsync();
        }
        public async Task<Poster> GetById(int id)
        {
            return await _db.Poster_tb.FindAsync(id);
        }
        public async Task<Poster> Add(Poster poster)
        {
            await _db.Poster_tb.AddAsync(poster);
            await _db.SaveChangesAsync();

            _eventService.CreateOrderEvent(new EventDataModel(poster.Poster_Id));

            return poster;
        }
        public async Task<string> Remove(int id)
        {
            var poster = await _db.Poster_tb.FindAsync(id);
            if (poster == null) return "no exsist any poster with poster_id " + id;
            _db.Poster_tb.Remove(poster);
            await _db.SaveChangesAsync();
            return $"the poster with Id's {id} removed succesfully";
        }
        public async Task<string> Update(Poster poster)
        {
            _db.Poster_tb.Update(poster);
            await _db.SaveChangesAsync();
            return $"the poster with Id's {poster.Poster_Id} Updated succesfully";
        }
        public async Task<PageResponse<Poster>> GetCompeletePostersAsync(PageRequest request)
        {
            var filterQuery = _db.Poster_tb.AsQueryable();

            #region Obtains
            
            if (request.FilterBy != null)
            {
                foreach (var item in request.FilterBy)
                {

                    if (item?.Field == Models.CompletedPosterFilterFiedls.Title)
                    {
                        filterQuery = filterQuery.Where(x => x.Title.Contains(item.Value)).AsQueryable();
                    }
                    //else if (item?.Field == Models.CompletedPosterSortingFiedls.Title)
                    //{
                    //    dataQuery = (item.Direction == Models.SortingDirection.ASC
                    //        ? dataQuery.OrderBy(x => x.Title)
                    //        : dataQuery.OrderByDescending(x => x.Title));
                    //}
                }
            }
           

            var dataQuery = filterQuery;


            if (request.First.HasValue)
            {
                if (!string.IsNullOrEmpty(request.After))
                {
                    int lastId = CursorHelper.FromCursor(request.After);
                    dataQuery = dataQuery.Where(x => x.Poster_Id > lastId);

                }
                dataQuery = dataQuery.Take(request.First.Value);
            }
            if (request.OrderBy != null)
            {
                foreach (var item in request.OrderBy)
                {

                    if (item?.Field == Models.CompletedPosterSortingFiedls.Id)
                    {
                        dataQuery = (item.Direction == Models.SortingDirection.ASC
                            ? dataQuery.OrderBy(x => x.Poster_Id)
                            : dataQuery.OrderByDescending(x => x.Poster_Id));
                    }
                    else if (item?.Field == Models.CompletedPosterSortingFiedls.Title)
                    {
                        dataQuery = (item.Direction == Models.SortingDirection.ASC
                            ? dataQuery.OrderBy(x => x.Title)
                            : dataQuery.OrderByDescending(x => x.Title));
                    }
                }
            }

            List<Poster> nodes = await dataQuery.ToListAsync();

            #endregion


            #region Obtians flags
            int maxId = nodes.Count !=0 ?  nodes.Max(x => x.Poster_Id) : 0;
            int minId = nodes.Count != 0 ? nodes.Min(x => x.Poster_Id): 0;
            bool hasNextPage = await filterQuery.AnyAsync(x => x.Poster_Id > maxId);
            bool hasPerviousPage = await filterQuery.AnyAsync(x => x.Poster_Id < minId);
            int totalCount = await filterQuery.CountAsync();
            #endregion

            return new PageResponse<Poster>
            {
                Nodes = nodes,
                HasNextPage = hasNextPage,
                HasPerviousPage = hasPerviousPage,
                TotalCount = totalCount
            };
        }

        public async Task<ILookup<int, Poster>> GetPostersByGroupIds(IEnumerable<int> groupIds)
        {
            var posters = await _db.Poster_tb.Where(a => groupIds.Contains(a.PosterGroup_Id)).ToListAsync();
            return posters.ToLookup(x => x.PosterGroup_Id);
        }

    }
}
