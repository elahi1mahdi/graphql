﻿using Graph_2.Models;
using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;

namespace Graph_2.Services
{
    public interface IEventService
    {
        IObservable<EventDataModel> OnCreateObservable { get; }

        void CreateOrderEvent(EventDataModel orderEvent);
    }

    public class EventService : IEventService
    {
        private readonly ISubject<EventDataModel> _onCreateSubject;
        public EventService()
        {
            _onCreateSubject = new ReplaySubject<EventDataModel>(1);
        }

        public void CreateOrderEvent(EventDataModel orderEvent) => _onCreateSubject.OnNext(orderEvent);
        public IObservable<EventDataModel> OnCreateObservable => _onCreateSubject.AsObservable();

    }
}
