﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Services.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
         Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> where = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
        Task<T> GetAsync(Expression<Func<T, bool>> where = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
        Task<T> GetByIdAsync(object Id);
        Task AddAsync(T entity);
        void Update(T entity);
        void Delete(T entity);
        void DeleteById(object Id);
        Task SaveChangesAsync();
        Task<bool> Exists(Expression<Func<T, bool>> where = null);
        Task<bool> IsSubGroup(object id);
         Task<IEnumerable<TResult>> GetWithIncludeAsync<TResult>(Expression<Func<T, TResult>> selector =null,
                                           Expression<Func<T, bool>> where = null,
                                           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                           Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
                                           bool disableTracking = true);
        Task<TResult> GetIncludeAsync<TResult>(Expression<Func<T, TResult>> selector = null,
                                          Expression<Func<T, bool>> where = null,
                                          Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                          Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
                                          bool disableTracking = true);

    }
}
