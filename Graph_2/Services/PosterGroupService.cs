﻿using DataLayer;
using Graph_2.Helper;
using Graph_2.Models;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace Graph_2.Services
{
    public interface IPosterGroupService
    {
        Task<PosterGroup> Add(PosterGroup poster);
        Task<IEnumerable<PosterGroup>> GetAll();
       PosterGroup GetById(int id);
        //Task<PageResponse<Poster>> GetCompeletePostersAsync(PageRequest request);
        Task<string> Remove(int id);
        Task<string> Update(PosterGroup poster);

        Task<IDictionary<int, PosterGroup>> GetGroupsById(IEnumerable<int> ids, CancellationToken cancellationToken);
    }
    public class PosterGroupService : IPosterGroupService
    {
        private readonly ApplicationDbContext _db;
        private readonly IEventService _eventService;
        public PosterGroupService(ApplicationDbContext db, IEventService eventService)
        {
            _eventService = eventService;
            _db = db;
        }

        public async Task<IEnumerable<PosterGroup>> GetAll()
        {
            return await _db.PosterGroup_tb.ToListAsync();
        }
        public PosterGroup GetById(int id)
        {
            return  _db.PosterGroup_tb.Find(id);
        }

        public async Task<IDictionary<int, PosterGroup>> GetGroupsById(IEnumerable<int> ids, CancellationToken cancellationToken)
        {
            return await _db.PosterGroup_tb.Where(c => ids.Contains(c.Id)).AsNoTracking().ToDictionaryAsync(c => c.Id);
        }

        public async Task<PosterGroup> Add(PosterGroup postergroup)
        {
            await _db.PosterGroup_tb.AddAsync(postergroup);
            await _db.SaveChangesAsync();

            //_eventService.CreateOrderEvent(new EventDataModel(poster.Poster_Id));

            return postergroup;
        }
        public async Task<string> Remove(int id)
        {
            var poster = await _db.PosterGroup_tb.FindAsync(id);
            if (poster == null) return "no exsist any posterGroup with id " + id;
           
            _db.PosterGroup_tb.Remove(poster);
            await _db.SaveChangesAsync();
            
            return $"the posterGroup with Id's {id} removed succesfully";
        }
        public async Task<string> Update(PosterGroup postergroup)
        {
            _db.PosterGroup_tb.Update(postergroup);
            await _db.SaveChangesAsync();
            return $"the posterGroup with Id's {postergroup.Id} Updated succesfully";
        }
        //public async Task<PageResponse<PosterGroup>> GetCompeletePostersAsync(PageRequest request)
        //{
        //    var filterQuery = _db.PosterGroup_tb.AsQueryable();

        //    #region Obtains
        //    if (request.FilterBy != null)
        //    {
        //        foreach (var item in request.FilterBy)
        //        {

        //            if (item?.Field == Models.CompletedPosterFilterFiedls.Title)
        //            {
        //                filterQuery = filterQuery.Where(x => x.Title.Contains(item.Value)).AsQueryable();
        //            }
        //            //else if (item?.Field == Models.CompletedPosterSortingFiedls.Title)
        //            //{
        //            //    dataQuery = (item.Direction == Models.SortingDirection.ASC
        //            //        ? dataQuery.OrderBy(x => x.Title)
        //            //        : dataQuery.OrderByDescending(x => x.Title));
        //            //}
        //        }
        //    }


        //    var dataQuery = filterQuery;


        //    if (request.First.HasValue)
        //    {
        //        if (!string.IsNullOrEmpty(request.After))
        //        {
        //            int lastId = CursorHelper.FromCursor(request.After);
        //            dataQuery = dataQuery.Where(x => x.Poster_Id > lastId);

        //        }
        //        dataQuery = dataQuery.Take(request.First.Value);
        //    }
        //    if (request.OrderBy != null)
        //    {
        //        foreach (var item in request.OrderBy)
        //        {

        //            if (item?.Field == Models.CompletedPosterSortingFiedls.Id)
        //            {
        //                dataQuery = (item.Direction == Models.SortingDirection.ASC
        //                    ? dataQuery.OrderBy(x => x.Poster_Id)
        //                    : dataQuery.OrderByDescending(x => x.Poster_Id));
        //            }
        //            else if (item?.Field == Models.CompletedPosterSortingFiedls.Title)
        //            {
        //                dataQuery = (item.Direction == Models.SortingDirection.ASC
        //                    ? dataQuery.OrderBy(x => x.Title)
        //                    : dataQuery.OrderByDescending(x => x.Title));
        //            }
        //        }
        //    }

        //    List<Poster> nodes = await dataQuery.ToListAsync();

        //    #endregion


        //    #region Obtians flags
        //    int maxId = nodes.Count !=0 ?  nodes.Max(x => x.Poster_Id) : 0;
        //    int minId = nodes.Count != 0 ? nodes.Min(x => x.Poster_Id): 0;
        //    bool hasNextPage = await filterQuery.AnyAsync(x => x.Poster_Id > maxId);
        //    bool hasPerviousPage = await filterQuery.AnyAsync(x => x.Poster_Id < minId);
        //    int totalCount = await filterQuery.CountAsync();
        //    #endregion

        //    return new PageResponse<Poster>
        //    {
        //        Nodes = nodes,
        //        HasNextPage = hasNextPage,
        //        HasPerviousPage = hasPerviousPage,
        //        TotalCount = totalCount
        //    };
        //}
    }
}
