﻿using GraphQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Graph_2.Extensions
{
    public static class GraphqlExtensions
    {
        /// <summary>
        /// A method that gets a value from a given input object with provided argument and field type name to match with.
        /// </summary>
        /// <param name="input">Provide an Inputs type.</param>
        /// <param name="argumentName">Provide name of the argument.</param>
        /// <param name="fieldTypeName">Provide field type name.</param>
        /// <returns>Returns a value from dictionary.</returns>
        public static string GetValue(this Inputs input, string argumentName, string fieldTypeName)
        {
            if (input.ContainsKey(argumentName))
            {
                var model = (Dictionary<string, object>)input[argumentName];
                if (model != null && model.ContainsKey(fieldTypeName))
                {
                    return model[fieldTypeName]?.ToString();
                }
            }

            return null;
        }
    }

    public static class StringExtensions
    {
        public static bool IsValidEmail(this string email) => Regex.IsMatch(email, @"^(([^<>()\[\]\.,;:\s@\""]+(\.[^<>()\[\]\.,;:\s@\""]+)*)|(\"".+\""))@(([^<>()[\]\.,;:\s@\""]+\.)+[^<>()[\]\.,;:\s@\""]{2,})$");
    }
}
