using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using DomainClasses.Role;
using DomainClasses.User;
using Graph_2.Enums;
using Graph_2.Helper;
using Graph_2.InputTypes;
using Graph_2.Models;
using Graph_2.Models.Auth;
using Graph_2.Mutaions;
using Graph_2.Query;
using Graph_2.Services;
using Graph_2.Subscription;
using Graph_2.Types;
using Graph_2.Types.Validators;
using GraphQL;
using GraphQL.Authorization;
using GraphQL.Server;
using GraphQL.Validation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualBasic;
using Services.Repositories;

namespace Graph_2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISServerOptions>(opt => {
                opt.AllowSynchronousIO = true;
            });
            services.Configure<KestrelServerOptions>(opt => {
                opt.AllowSynchronousIO = true;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
           options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")).EnableSensitiveDataLogging(), ServiceLifetime.Transient);
            services.AddTransient<ApplicationDbContext>();

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
            }).AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();


            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "https://localhost:44362",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("this is secret key graph"))
                };
            });

            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));

            services.AddControllers();

            services.AddScoped<IServiceProvider>(c => new FuncServiceProvider(type => c.GetRequiredService(type)));
            services.AddScoped<IEventService, EventService>();

            services.AddTransient<IPosterService, PosterService>();
            services.AddTransient<IPosterGroupService, PosterGroupService>();

            services.AddTransient<PosterType>();
            services.AddTransient<PosterGroupType>();
            services.AddTransient<EventDataType>();
            
            services.AddTransient<CompletedPosterSortingFiedlsEnumType>();
            services.AddTransient<SortingDirectionEnumType>();

            services.AddTransient<CompletedPosterOrderByInputType>();
            services.AddTransient<PosterInputType>();

            
            services.AddTransient<PosterQuery>();
            services.AddTransient<PosterSchema>();
            services.AddTransient<PosterMutaion>();
            services.AddTransient<PosterSubscription>();

            services.AddTransient<PosterGroupQuery>();
            services.AddTransient<PosterGroupSchema>();

            services.TryAddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddTransient<IAuthorizationEvaluator, AuthorizationEvaluator>();
            //services.TryAddTransient<IValidationRule, AuthValidationRule>();

            services.AddTransient(_ =>
            {
                var authSetting = new AuthorizationSettings();

                authSetting.AddPolicy(Constant.AuthPolicy.AdminPolicy,
                    policy => policy.RequireClaim(ClaimTypes.Role, Constant.Roles.Admin));

                authSetting.AddPolicy(Constant.AuthPolicy.CustomerPolicy,
                     policy => policy.RequireClaim(ClaimTypes.Role, Constant.Roles.Customer));

                return authSetting;
            });

            services.AddGraphQL(opt =>
            {
                opt.EnableMetrics = true;
                opt.ExposeExceptions = false;
                opt.UnhandledExceptionDelegate = context =>
                {
                    Console.WriteLine("Error :" + context.OriginalException.Message);
                };
            }).AddUserContextBuilder(httpContext => new GraphQLUserContext { User = httpContext.User})
                .AddWebSockets().AddDataLoader().AddGraphTypes(typeof(PosterSchema));

         
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseAuthentication();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseWebSockets();
            
            app.UseGraphQL<PosterSchema>();

            app.UseGraphQLWebSockets<PosterSchema>();

            app.UseGraphQLPlayground();
        }
    }
}
