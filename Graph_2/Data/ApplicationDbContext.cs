﻿
using DomainClasses.Role;
using DomainClasses.User;
using Graph_2.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace DataLayer
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

      
        public DbSet<Poster> Poster_tb { get; set; }
        public DbSet<PosterGroup> PosterGroup_tb { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
          

            base.OnModelCreating(builder);

        }

    }
}
