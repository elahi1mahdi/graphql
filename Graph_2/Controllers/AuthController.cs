﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using DomainClasses.User;
using Graph_2.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Services.Repositories;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        public IGenericRepository<ApplicationUser> _userRepository;
        public UserManager<ApplicationUser> _userManager;
        public SignInManager<ApplicationUser> _signInManager;
        public IHttpContextAccessor _tokenContext;
        public AuthController(IGenericRepository<ApplicationUser> userRepository,
          IHttpContextAccessor tokenContext,
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenContext = tokenContext;
            _userRepository = userRepository;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]   LoginViewModel login)

        {
          
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Check User
            var user = await _userRepository.GetAsync(x => x.UserName == login.Username);
            if (user == null || !await _userManager.IsInRoleAsync(user, "Customer")) return BadRequest(new { username =  "نام کاربری یا رمز عبور اشتباه می باشد" });
            //Check Pass

            var result = await _userManager.CheckPasswordAsync(user, login.Password);
            if (!result) return BadRequest(new { username = "نام کاربری یا رمز عبور اشتباه می باشد" });

            var profile = new ProfileViewModel { FullName = user.FullName, Mobile = user.UserName };

            return Ok(new { token = await CreateToken(user), profile = profile });
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel viewModel)

        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = await _userRepository.GetAsync(x => x.UserName == viewModel.Username);

            if (user != null)
                return BadRequest(new { mobile = " شماره همراه قبلا ثبت شده است" });


            var new_user = new ApplicationUser()
            {
                FullName = "کاربر جدید",
                Email = "",
                UserName = viewModel.Username,
                Mobile_1 = viewModel.Username,
                SecurityStamp = Guid.NewGuid().ToString(),
                EmailConfirmed = true
            };
            var result = await _userManager.CreateAsync(new_user, viewModel.Password);

            await _userManager.AddToRoleAsync(new_user, "Customer");

            return Ok(new { message = "کد امنیتی تائید شد", token = await CreateToken(new_user)});
        }
       
        public async Task<string> CreateToken(ApplicationUser user)
        {
            var handler = new JwtSecurityTokenHandler();

            
                    var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("this is secret key graph"));
                    var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                    var tokenOption = new JwtSecurityToken(
                        issuer: "https://itisa.abroon.net",
                        claims: new List<Claim>
                        {
                    new Claim(ClaimTypes.Name,user.UserName),
                    new Claim(ClaimTypes.Role,"Customer")
                        }
                        ,
                        expires: DateTime.Now.AddYears(1),
                        

                        signingCredentials: signinCredentials
                        );

                    var new_token = new JwtSecurityTokenHandler().WriteToken(tokenOption);

                    _userRepository.Update(user);
                    await _userRepository.SaveChangesAsync();

                    return new_token;
        }
      

      

    }
}