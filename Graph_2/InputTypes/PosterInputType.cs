﻿using Graph_2.Models;
using Graph_2.Types.Validators;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.InputTypes
{
    public class PosterInputType:InputObjectGraphType<Poster>
    {
        public PosterInputType()
        {
            Name = nameof(PosterInputType);

            Field(x => x.Poster_Id, nullable: true) ;
            Field(x => x.Price);
            Field(x => x.Title);
            Field(x => x.Description);
            Field(x => x.PosterGroup_Id);
        }
    }
}
