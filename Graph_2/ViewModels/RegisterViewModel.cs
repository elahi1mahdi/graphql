﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Graph_2.ViewModels
{
    public class RegisterViewModel
    {
        public RegisterViewModel()
        {

        }
        [Required(ErrorMessage = "{0} خود را وارد کنید")]
        [StringLength(100, ErrorMessage = "رمز عبور شما باید حداقل 6 کاراکتر باشد", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "رمز عبور")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تکرار رمز عبور")]
        [Compare("Password", ErrorMessage = "{0} یکسان نیست")]
        public string ConfirmPassword { get; set; }
        
        [Required(ErrorMessage = " {0} را وارد کنید")]
        [Display(Name = "تلفن همراه")]
        [RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "تلفن همراه وارد شده نامتعبر است")]
        public string Username { get; set; }
    }
}
