﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Graph_2.ViewModels
{
    public class LoginViewModel
    {
        public LoginViewModel()
        {

        }
        [Required(ErrorMessage = " {0} را وارد کنید")]
        [Display(Name = "نام کاربری")]
        public string Username { get; set; }

        [Display(Name = "رمز عبور")]
        [Required(ErrorMessage = "رمز عبور خود را وارد کنید")]

        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}
