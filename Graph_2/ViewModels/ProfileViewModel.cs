﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Graph_2.ViewModels
{
    public class ProfileViewModel
    {
        public ProfileViewModel()
        {

        }

        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "{0} نامعتبر می باشد")]
        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "{0} را وارد کنید")]

        public string Email { get; set; }


        [Display(Name = "نام و نام خانوادگی")]
        [Required(ErrorMessage = "{0} را وارد کنید")]
        [StringLength(100, ErrorMessage = "{0} حداکثر باید 100 کاراکتر باشد")]
        public string FullName { get; set; }

        public string Mobile { get; set; }

    }
}
