﻿using Graph_2.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Enums
{
    public class CompletedPosterSortingFiedlsEnumType:EnumerationGraphType<CompletedPosterSortingFiedls>
    {
        public CompletedPosterSortingFiedlsEnumType()
        {
            Name = nameof(CompletedPosterSortingFiedlsEnumType);
        }
    }
}
