﻿using Graph_2.Models;
using Graph_2.Services;
using GraphQL.DataLoader;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Types
{
    public class PosterGroupType : ObjectGraphType<PosterGroup>
    {
        public PosterGroupType(IPosterService posterService,IDataLoaderContextAccessor dataLoader)
        {
            Name = nameof(PosterGroupType);

            Field(x => x.Id);
            Field(x => x.Title);
            Field<ListGraphType<PosterType>>(
           "posters",
           resolve: context =>
           {
               var loader = dataLoader.Context.GetOrAddCollectionBatchLoader<int, Poster>("GetPostersByGroupIds", posterService.GetPostersByGroupIds);
               return loader.LoadAsync(context.Source.Id);
           });
        }
     
    }
}
