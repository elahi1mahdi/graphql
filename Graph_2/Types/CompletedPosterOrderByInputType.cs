﻿using Graph_2.Enums;
using Graph_2.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Types
{
    public class CompletedPosterOrderByInputType:InputObjectGraphType<SortingDetails<CompletedPosterSortingFiedls>>
    {
        public CompletedPosterOrderByInputType()
        {
            Field<CompletedPosterSortingFiedlsEnumType>("field", resolve: context => context.Source.Field);
            Field<SortingDirectionEnumType>("direction", resolve: context => context.Source.Direction);

        }
    }
}
