﻿using Graph_2.Models;
using Graph_2.Services;
using GraphQL.DataLoader;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Types
{
    public class PosterType : ObjectGraphType<Poster>
    {
        public PosterType(IPosterService posterService, IPosterGroupService posterGroupService, IDataLoaderContextAccessor dataLoader)
        {
            Name = nameof(PosterType);

            Field(x => x.Poster_Id);
            Field(x => x.Title);
            Field(x => x.Description);
            Field(x => x.Price);
            Field(x => x.PosterGroup_Id);
            Field<PosterGroupType, PosterGroup>()
                 .Name("posterGroup")
                 .ResolveAsync(ctx =>
                 {
                     var loader = dataLoader.Context.GetOrAddBatchLoader<int, PosterGroup>("GetGroupsById", posterGroupService.GetGroupsById);
                     return loader.LoadAsync(ctx.Source.PosterGroup_Id);
                 });

        }
    }
}
