﻿using Graph_2.Extensions;
using GraphQL;
using GraphQL.Language.AST;
using GraphQL.Types;
using GraphQL.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Graph_2.Types.Validators
{
    public class EmailAddressValidationRule : IValidationRule
    {
        public INodeVisitor Validate(ValidationContext context)
        {
            return new EnterLeaveListener(_ =>
            {
                _.Match<Argument>(argAst =>
                {
                    var argDef = context.TypeInfo.GetArgument();
                    if (argDef == null) return;

                    var type = argDef.ResolvedType;
                    if (type.IsInputType())
                    {
                        var fields = ((type as NonNullGraphType)?.ResolvedType as IComplexGraphType)?.Fields;
                        if (fields != null)
                        {
                            //let's look for fields that have a specific metadata
                            foreach (var fieldType in fields.Where(f => f.HasMetadata(nameof(EmailAddressValidationRule))))
                            {
                                //now it's time to get the value
                                var value = context.Inputs.GetValue(argAst.Name, fieldType.Name);
                                if (value != null)
                                {
                                    if (!value.IsValidEmail())
                                    {
                                        context.ReportError(new ValidationError(context.OriginalQuery
                                            , "Invalid Email Address"
                                            , "The supplied email address is not valid."
                                            , argAst
                                        ));
                                    }
                                }
                            }
                        }
                    }
                });
            });
        }
       
    }
}
