﻿using Graph_2.Enums;
using Graph_2.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Types
{
    public class CompletedPosterFilterByInputType : InputObjectGraphType<FilteringDetails<CompletedPosterFilterFiedls>>
    {
        public CompletedPosterFilterByInputType()
        {
            Field<CompletedPosterSortingFiedlsEnumType>("field", resolve: context => context.Source.Field);
            Field<StringGraphType>("value", resolve: context => context.Source.Value);
        }
    }
}
