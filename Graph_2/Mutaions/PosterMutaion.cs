﻿using Graph_2.InputTypes;
using Graph_2.Models;
using Graph_2.Services;
using Graph_2.Types;
using GraphQL;
using GraphQL.Types;
using GraphQL.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graph_2.Mutaions
{
    
    public class PosterMutaion : ObjectGraphType
    {
        public PosterMutaion(IPosterService posterService)
        {
            Name = nameof(PosterMutaion);
            FieldAsync<PosterType>(
                name: "createPoster",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<PosterInputType>> { Name = "poster" }),
                resolve: async context =>
                 {
                     var poster = context.GetArgument<Poster>("poster");
                     return await posterService.Add(poster);
                 });


            FieldAsync<StringGraphType>(
          name: "updatePoster",
          arguments: new QueryArguments(new QueryArgument<NonNullGraphType<PosterInputType>> { Name = "poster" },
          new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "id" }),
          resolve: async context =>
          {

              var new_model = context.GetArgument<Poster>("poster");
              var old_model = await posterService.GetById(context.GetArgument<int>("id"));

              
              if (old_model == null)
              {
                  context.Errors.Add(new ExecutionError("Couldn't find Tech event info."));
                  return null;
              }

              old_model.Title = new_model.Title;
              old_model.Description = new_model.Description;
              old_model.Price = new_model.Price;
              old_model.PosterGroup_Id = new_model.PosterGroup_Id;

              return await posterService.Update(old_model);
          });




            FieldAsync<StringGraphType>(
            name: "removePoster",
            arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "id" }),
            resolve: async context =>
            {
           


                return await posterService.Remove(context.GetArgument<int>("id"));
            });
        }

    }
}
